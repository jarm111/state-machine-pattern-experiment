# State Machine Pattern Experiment
Small platformer experiment project with state machine design pattern.

## Features
- Simple plaformer controls
- Empty stage
- Player can move, jump and crouch
- Controls are managed by state machine
- Hud displays current state